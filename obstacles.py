import random

from fg_elements import fg_element
from globals import *
from colorama import Fore, Back, Style

class pipe(fg_element):
    '''
        Class for pipes that inherits move and draw functions from fg_elements.
    '''

    def __init__(self, size=3):
        '''
            Args:
                size : integer, size of pipe

            Returns:
                None
        '''
        super(pipe, self).__init__(SCREEN_WIDTH + 1, SKY_SIZE - size + 1)
        self.size = size
        self.width = 3
        self.chars = [[Back.BLUE+' '+Back.RESET for i in range(5)] \
        for j in range(size)]

class enemy(fg_element):
    '''
        Class for enemies that overrides the move method of fg_element, and has
        functionality for checking if the enemy collides with Mario.
    '''
    def __init__(self, v=1):
        '''
            Args:
                v : integer, speed the enemy moves at

            Returns:
                None
        '''
        # Enemy always starts at the same place
        super(enemy, self).__init__(SCREEN_WIDTH - 1, SKY_SIZE)
        self.alive = True
        self.vel = -1*v
        self.chars = [[Fore.MAGENTA + '+' + Fore.RESET, Fore.MAGENTA + 'O' + Fore.RESET, Fore.MAGENTA + '+' + Fore.RESET]]

    def move(self, dir, pipes):
        '''
            Move the enemy automatically, bouncing between pipes.

            Args:
                None

            Returns:
                None
        '''
        for pipe in pipes:
            if (pipe.x - (self.x + 2) == 0 and self.vel > 0) or (self.x - (pipe.x + 5) == 0 and self.vel < 0):
                self.vel *= -1
        if self.x >= SCREEN_WIDTH and self.vel > 0:
            self.vel *= -1
        self.x += self.vel
        self.x += dir

    def check_collision(self, mario):
        '''
            Check if the enemy collides with Mario. There are two types of
            collisions, each with different effects.

            Args:
                None

            Returns:
                None
        '''
        if abs(self.x - mario.x) <= 1 and mario.y + mario.height >= SKY_SIZE:
            # If the enemy attacks Mario from the size
            if mario.vel_y == 0:
                return True
            else:
                # If Mario lands on the enemy from above
                self.alive = False
                mario.points += 50
        return False

class coin(fg_element):
    '''
        Class for coins that inherits from fg_element, and has functionality
        for checking if Mario has collected the coin.
    '''
    def __init__(self, x=SCREEN_WIDTH + 1, y=SKY_SIZE):
        '''
            Args:
                x : horizontal coordinate, integer
                y : vertical coordinate, integer

            Returns:
                None
        '''
        super(coin, self).__init__(x, y)
        self.collected = False
        self.reward = 10
        self.chars = [[Back.YELLOW + Fore.BLACK + Style.BRIGHT + '$' + Style.RESET_ALL + Fore.RESET + Back.RESET]]

    def check_contact(self, mario):
        '''
            Check if Mario collects the coin.

            Args:
                mario : Mario object

            Returns:
                None
        '''
        if abs(self.x - mario.x) <= 1 and mario.y <= self.y <= mario.y + mario.height and not self.collected:
            mario.points += self.reward
            self.collected = True

class supercoin(coin):
    '''
        Supercoin that inherits from coin, and gives additional points when
        collected.
    '''
    def __init__(self, x=SCREEN_WIDTH + 1, y=SKY_SIZE):
        '''
            Args:
                x : horizontal coordinate, integer
                y : vertical coordinate, integer

            Returns:
                None
        '''
        super(supercoin, self).__init__(x, y)
        self.chars = [[Back.YELLOW + Fore.MAGENTA + Style.BRIGHT + '$' + Style.RESET_ALL + Fore.RESET + Back.RESET]]
        self.reward = 30
