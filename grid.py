from colorama import Fore, Back, Style
from globals import *
from foreground import Foreground
from background import Background

def bg_set(s):
    '''
        Check if the background colour is set in s.

        Args:
            s : string

        Returns:
            Boolean, True if background is set, False otherwise
    '''
    if not s:
        return False
    bg_chars = [Back.BLACK, Back.RED, Back.GREEN, Back.YELLOW, Back.BLUE,\
    Back.MAGENTA, Back.CYAN, Back.WHITE]
    for i in bg_chars:
        if i in s:
            return True
    return False

class Grid(object):
    '''
        Class to store both foreground and background grids, and draw and update both before blitting on screen.
    '''
    def __init__(self, fg=None, bg=None, level=20):
        '''
            Args:
                fg : foreground object
                bg : background object
                level : number of pipes before level is done, integer

            Returns:
                None
        '''
        self.grid = [[None for j in range(SCREEN_WIDTH)] for i in range(SCREEN_HEIGHT)]
        self.level = level
        self.fg = fg
        self.bg = bg

    def update(self, dir, jump):
        '''
            Update foreground and background.

            Args:
                dir : direction and velocity of movement

            Returns:
                Boolean, whether level is done or not
        '''
        # foreground update function returns boolean, indicating if Mario's path
        # is blocked by a pipe
        blocked = self.fg.update(dir, jump)
        if self.fg.n_pipes >= self.level:
            return True

        # If path is blocked, don't move the background forward
        if blocked:
            dir = 0
        self.bg.update(dir)
        return False

    def draw(self):
        '''
            Draw background first, and then draw foreground over it.

            Args:
                None

            Returns:
                None
        '''
        self.fg.draw_grid()
        self.bg.draw_grid()

        for i in range(SCREEN_HEIGHT):
            for j in range(SCREEN_WIDTH):
                self.grid[i][j] = self.bg.grid[i][j]
                if bg_set(self.fg.grid[i][j]):
                    self.grid[i][j] = self.fg.grid[i][j]
                elif self.fg.grid[i][j]:
                    self.grid[i][j] = self.bg.grid[i][j] + self.fg.grid[i][j] +\
                    Back.RESET
                else:
                    self.grid[i][j] = self.bg.grid[i][j] + ' ' + Back.RESET

    def get_score(self):
        '''
            Get the score for Mario.

            Args:
                None

            Returns:
                integer, score
        '''
        return self.fg.mario.points
