import time
import signal
import sys
import termios
import tty
import os

from globals import *
from input import *
from background import Background
from foreground import Foreground
from grid import Grid
from mario import Mario

def level_up(level, score):
    os.system('clear')
    if level == 2:
        print("GAME COMPLETE!")
    else:
        print("LEVEL COMPLETE!")
    print("Score = " + str(score))
    time.sleep(3)

def draw(g, rem_time):
    sys.stdout.flush()
    os.system('clear')
    sys.stdout.write("Time remaining: " + str(rem_time) + '\t\t\tScore: '+str(g.get_score()) + '\n')
    for col in range(SCREEN_WIDTH + 2):
        sys.stdout.write('-')
    sys.stdout.write('\n')

    for i in range(SCREEN_HEIGHT):
        sys.stdout.write('|')
        for j in range(SCREEN_WIDTH):
            sys.stdout.write(g.grid[i][j])
        sys.stdout.write('|\n')

    for col in range(SCREEN_WIDTH + 2):
        sys.stdout.write('-')
    sys.stdout.write('\n')

def play(level=1, init_score=0):

    M = Mario(SCREEN_WIDTH // 2, SKY_SIZE - 2, init_score)
    bg = Background()
    if level == 1:
        fg = Foreground(M, [0], 1)
    else:
        fg = Foreground(M, [0, 1, 2], 1)
    g = Grid(fg, bg, level * 20)
    start_time = time.time()
    vel = 0
    acc = 0
    jump = False
    finish = False
    while True:
        rem_time = int(100 - (time.time() - start_time))
        if rem_time <= 0:
            game_over(g.get_score())
        #print(vel, acc)
        command = get_keypress(getch)
        if command == 'q':
            game_over(g.get_score())
        elif command == 'a':
            vel = 2
            # acc = -0.1
        elif command == 'd':
            vel = -2
            # acc = 0.1
        elif command == 'w':
            jump = True
        finish = g.update(int(vel), jump)
        jump = False
        # vel = min(0, vel + acc)
        g.draw()
        vel = 0
        draw(g, rem_time)
        if finish:
            level_up(level, g.get_score())
            return g.get_score()
        time.sleep(0.05)

if __name__ == '__main__':
    score = play(1)
    play(2, score)
