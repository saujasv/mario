import sys

import random
import time
from colorama import Fore, Back, Style
from globals import *
from fg_elements import *
from obstacles import *

class Foreground(object):
    '''
        Class for storing and simulating interaction between all foreground
        elements.
    '''
    def __init__(self, mario=None, freq=[1], enemy_speed=1):
        '''
            Args:
                mario: Mario object
                freq: list for determining frequency of generating enemies
                enemy_speed : integer, speed at which enemy moves

            Returns:
                None
        '''
        self.width = SCREEN_WIDTH
        self.height = SCREEN_HEIGHT
        self.__sky_size = SCREEN_HEIGHT * 3 // 4
        self.__ground_size = SCREEN_HEIGHT - self.__sky_size

        # counting the number of pipes that have been spawned
        self.n_pipes = 0
        self.pipes = list()
        self.enemies = list()
        self.coins = list()
        self.freq = freq
        self.mario = mario
        self.enemy_speed = enemy_speed
        self.grid = [[None for j in range(SCREEN_WIDTH)] for i in\
        range(SCREEN_HEIGHT)]


    def spawn_pipe(self):
        '''
            Spawn pipes when enough distance has passed from the previous pipe.

            Args:
                None

            Returns:
                None
        '''
        if len(self.pipes) > 0 and self.n_pipes <= 50:
            if self.pipes[-1].x <= random.randint(SCREEN_WIDTH // 3, \
            2 * SCREEN_WIDTH // 3):
                self.pipes.append(pipe(random.randint(3, 7)))
                # Randonly place supercoins on top of pipes
                if random.randint(0, 4) == 2:
                    self.coins.append(supercoin(self.pipes[-1].x,\
                    self.pipes[-1].y - 2))
                    self.coins.append(supercoin(self.pipes[-1].x + 1,\
                    self.pipes[-1].y - 2))
                    self.coins.append(supercoin(self.pipes[-1].x + 2,\
                    self.pipes[-1].y - 2))
                self.n_pipes += 1

                # Spawn an enemy randomly to move between pipes
                if random.randint(0, 2) in self.freq:
                    self.spawn_enemy()
        else:
            self.pipes.append(pipe(random.randint(3, 7)))

    def spawn_enemy(self):
        '''
            Add an enemy ot the list.

            Args:
                None

            Returns:
                None
        '''
        self.enemies.append(enemy(self.enemy_speed))

    def spawn_coins(self):
        '''
            Spawn coins in two places -- on the ground and in the air.

            Args:
                None

            Returns:
                None
        '''
        # Coins on the ground
        if random.randint(0, 1) == 1:
            self.coins.append(coin())

        # Coins in the air
        if random.randint(0, 20) == 1:
            self.coins.append(coin(random.randint(SKY_SIZE - 15, SKY_SIZE - 5)))


    def draw_grid(self):
        '''
            Draw all the elements on the foreground grid.

            Args:
                None

            Returns:
                None
        '''
        for i in range(SCREEN_HEIGHT):
            for j in range(SCREEN_WIDTH):
                self.grid[i][j] = None

        self.mario.draw_on_grid(self)

        for coin in self.coins:
            if not coin.collected:
                coin.draw_on_grid(self)

        for enemy in self.enemies:
            if enemy.alive:
                enemy.draw_on_grid(self)

        for pipe in self.pipes:
            pipe.draw_on_grid(self)


    def update(self, dir, jump):
        '''
            Move all foreground elements.

            Args:
                None

            Returns:
                None
        '''
        self.mario.move(dir, jump, self.pipes)

        for enemy in self.enemies:
            enemy.move(dir, self.pipes)
            if enemy.alive:
                collision = enemy.check_collision(self.mario)
                if collision:
                    game_over(self.mario.points)
                    pass

        if self.mario.pipe_in_front(dir, self.pipes):
            return True

        self.spawn_pipe()
        self.spawn_coins()

        for pipe in self.pipes:
            pipe.move(dir, jump)

        for coin in self.coins:
            coin.check_contact(self.mario)
            coin.move(dir, jump)

        self.clear_elements()

        return False

    def clear_elements(self):
        '''
            Remove elements that have passed outside the left of the screen.

            Args:
                None

            Returns:
                None
        '''
        if len(self.pipes) > 0 and self.pipes[0].out_of_screen():
            self.pipes.pop(0)
