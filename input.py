import time
import signal
import sys
import termios
import tty
import os

def getch():
        '''
            Read a single keypress from stdin and return the resulting character.

            Args:
                None

            Returns:
                1 element string, character that is read
        '''
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

class timeoutException(Exception):
    '''
        Class to raise a custom exception for an input timeout.
    '''
    pass

def time_up(signum, frame):
    '''
        Raise a timeoutException if called.

        Args:
            None

        Returns:
            None
    '''
    raise timeoutException

def get_keypress(getch, timeout=0.05):
    '''
        Get the single character input, or return no input.

        Args:
            None

        Returns:
            1 element string, if character is read, or empty string
    '''
    signal.signal(signal.SIGALRM, time_up)
    signal.setitimer(signal.ITIMER_REAL, timeout)
    try:
        inp = getch()
        signal.alarm(0)
        return inp
    except timeoutException:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)
        return ''
