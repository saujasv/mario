import random
from colorama import Fore, Back, Style
from globals import *
from foreground import fg_element

class Mario(fg_element):
    '''
        Class for Mario that has functions to move Mario and make him jump, and
        check if there is a pipe in front of Mario.
    '''

    # Mario when on the ground, or when on the way down
    std_mario = [['_', Back.YELLOW + Fore.BLACK + 'M' + Fore.RESET +\
    Back.RESET, '_'],
    [Fore.RED + '/' + Fore.RESET, Back.RED + Fore.BLUE + '#' + Fore.RESET +\
    Back.RESET, Fore.RED + '\\' + Fore.RESET],
    [Fore.BLUE+'/'+Fore.RESET, '_', Fore.BLUE+'\\'+Fore.RESET]]

    # Mario on the way up during a jump
    jump_mario = [[Fore.RED + '\\' + Fore.RESET, Back.YELLOW + Fore.BLACK + \
    'M' + Fore.RESET + Back.RESET, Fore.RED + '/' + Fore.RESET],
    ['_', Back.RED + Fore.BLUE + '#' + Fore.RESET + Back.RESET, '_'],
    [Fore.BLUE+'/'+Fore.RESET, '_', Fore.BLUE+'\\'+Fore.RESET]]

    def __init__(self, x=SCREEN_WIDTH // 2 - 1, y=SCREEN_HEIGHT // 2, points=0):
        '''
            Args:
                x : horizontal coordinate, integer
                y : vertical coordinate, integer
                points : initial score, integer

            Returns:
                None
        '''
        super(Mario, self).__init__(x, y)
        self.height = 3
        self.width = 3
        self.chars = Mario.std_mario

        self.upward_acc = -2
        self.vel_y = 0
        self.points = points

    def pipe_in_front(self, dir, pipes):
        '''
            Check if there is a pipe in front of Mario.

            Args:
                dir : direction of movement and velocity, integer
                pipes : ist of pipes

            Returns:
                Boolean : True if there is a pipe in front, False otherwise
        '''
        if dir <= 0:
            for pipe in pipes:
                if (self.x + 1 - dir == pipe.x) and \
                self.y + self.height >= SKY_SIZE - pipe.size + 1:
                    return True
        else:
            for pipe in pipes:
                if (self.x - dir == pipe.x + 3) and \
                self.y + self.height >= SKY_SIZE - pipe.size + 1:
                    return True
        return False


    def move(self, dir, jump, pipes):
        '''
            Move Mario on the screen, with functionality to jump on pipes.

            Args:
                dir : direction and velocity of movement, integer
                jump : whether Mario is jumping off the ground or not, boolean
                pipes : list of pipes

            Returns:
                None
        '''
        if jump:
            self.upward_acc = (SKY_SIZE - self.y) / SKY_SIZE
            self.chars = Mario.jump_mario
        else:
            self.upward_acc = -0.5

        # If over a pipe, set ground to height of pipe so Mario doesn't go below
        # that height
        ground = SKY_SIZE + 1
        for pipe in pipes:
            if pipe.x <= self.x <= pipe.x + 3 or \
            pipe.x <= self.x + 1 <= pipe.x + 3 or \
            pipe.x <= self.x + 2 <= pipe.x + 3:
                ground -= pipe.size

        # Update position and velocity
        if (not jump) and self.y >= ground - self.height:
            self.vel_y = 0
        else:
            self.vel_y += min(3, self.upward_acc)
            self.y = min(int(self.y - self.vel_y), ground - self.height)

        if self.y <= 0:
            self.vel_y *= -1
            self.y += 1

        # Change form of Mario based on direction of vertical movement
        if self.vel_y <= 0:
            self.chars = Mario.std_mario
        else:
            self.chars = Mario.jump_mario
