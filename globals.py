import time
import os
import sys

'''
    File to set some global constants and values that need to be available
    everywhere.
'''

SCREEN_WIDTH = 80
SCREEN_HEIGHT = 30

SKY_SIZE = SCREEN_HEIGHT * 3 // 4
GROUND_SIZE = SCREEN_HEIGHT - SKY_SIZE

def game_over(score):
    '''
        Print the game over message and score, and exit the program after 5 seconds.

        Args:
            score : integer

        Returns:
            None
    '''
    os.system('clear')
    print("GAME OVER")
    print("Final score = " + str(score))
    time.sleep(5)
    sys.exit()
