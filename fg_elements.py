import random
from colorama import Fore, Back, Style
from globals import *

class fg_element(object):
    '''
        Base class for all foreground elements.

        Specifies methods to move, check if element is out of screen, draw
        elements on grid.
    '''
    def __init__(self, x=SCREEN_WIDTH // 2, y=SCREEN_HEIGHT // 2):
        '''
            Args:
                x : horizontal coordinate, integer
                y : vertical coordinate, integer

            Returns:
                None
        '''
        self.x = x
        self.y = y

    def move(self, dir, jump):
        '''
            Move a foreground element by dir.

            Args:
                dir : direction and velocity of movement, integer
                jump : whether Mario is jumping or not, Boolean

            Returns:
                None
        '''
        self.x += dir

    def out_of_screen(self):
        '''
            Checks if the entire element is out of the screen.

            Args:
                None

            Returns:
                None
        '''
        if self.x < -self.width:
            return True
        else:
            return False

    def draw_on_grid(self, foreground):
        '''
            Draws an element onto the foreground grid.

            Args:
                element: hill or cloud object

            Returns:
                None
        '''

        for i in range(len(self.chars)):
            for j in range(len(self.chars[0])):
                # Handle parts of the element outside the screen
                if (not 0 <= j + self.x < SCREEN_WIDTH) or (not 0 <= self.y < SCREEN_HEIGHT):
                    continue

                try:
                    if self.chars[i][j] != '_':
                        foreground.grid[self.y + i][self.x + j] = self.chars[i][j]
                except:
                    pass
