import random
from colorama import Fore, Back, Style
from bg_elements import cloud_types, hill_types
from globals import *

class background_element(object):
    '''
        Base class for background elements -- hills and clouds -- that specifies
        the horizontal position on the screen, the move function, and out of
        screen check function.

        Having the move function implemented in the base class allows the
        background to be moved at a uniform rate.
    '''
    def __init__(self):
        '''
            Args:
                None
        '''
        # Starting horizontal coordinate
        self.x = SCREEN_WIDTH + 1

    def move(self, dir):
        '''
            Args:
                dir : integer

            Returns:
                None

            Sets the new horizontal location of the background element when
            the player moves. dir can specify both speed and direction of
            movement, with direction being forward if dir is negative and
            backward if dir is positive.
        '''

        self.x += dir

    def out_of_screen(self):
        '''
            Args:
                None

            Returns:
                None

            Checks if the entire element is out of the screen.
        '''
        if self.x < -self.width:
            return True
        else:
            return False

    def draw_on_grid(self, background):
        '''
            Draws an element onto self.grid.

            Args:
                element: hill or cloud object

            Returns:
                None
        '''
        chars = str(self).split('\n')

        for i in range(len(chars)):
            for j in range(len(chars[0])):
                # Handle parts of the element outside the screen
                if j + self.x >= SCREEN_WIDTH or j + self.x < 0:
                    continue

                if chars[i][j] == '#':
                    background.grid[self.y + i][self.x + j] = self.colour

class cloud(background_element):
    '''
        Class for background clouds. Specifies methods for initialisation,
        and information required for printing cloud on screen.
    '''
    n_types = len(cloud_types)
    __h_lim = SCREEN_HEIGHT / 3
    colour = Back.WHITE

    def __init__(self, type):
        '''
            Args:
                type : Integer between 0 and n_types - 1 (inclusive)
        '''
        super(cloud, self).__init__()

        # Choose shape of cloud from preset shapes available in bg_elements
        self.__string = cloud_types[type]

        # Determine width of cloud
        self.width = len(self.__string.split('\n')[0])

        # Determine initial vertical coordinate of cloud
        # This does not change
        self.y = random.randint(0, cloud.__h_lim)

    def __repr__(self):
        '''
            Returns string form of cloud object.
        '''
        return self.__string

class hill(background_element):
    n_types = len(hill_types)
    __h_lower_lim = SCREEN_HEIGHT / 3
    __h_upper_lim = SCREEN_HEIGHT * 3 // 4
    colour = Back.GREEN

    def __init__(self, type):
        '''
            Args:
                type : Integer between 0 and n_types - 1 (inclusive)
        '''
        super(hill, self).__init__()

        # Choose shape of hill from preset shapes available in bg_elements
        self.__string = hill_types[type]

        # Determine width of hill
        self.width = len(self.__string.split('\n')[0])

        # Initial vertical coordinate depends on height of hill since hills are
        # grounded
        self.y = hill.__h_upper_lim - len(self.__string.split('\n')) + 1

    def __repr__(self):
        return self.__string


class Background(object):
    '''
        Class for entire background. Has methods for spawning new background
        elements, updating location of background elements, and drawing the
        background onto a 2D array.
    '''
    sky_colour = Back.CYAN
    ground_colour = Back.RED

    def __init__(self):
        '''
            Args:
                None
        '''
        self.width = SCREEN_WIDTH
        self.height = SCREEN_HEIGHT
        self.__sky_size = SCREEN_HEIGHT * 3 // 4
        self.__ground_size = SCREEN_HEIGHT - self.__sky_size
        self.__clouds = list()
        self.__hills = list()
        self.grid = [[None for j in range(SCREEN_WIDTH)] for i in range(SCREEN_HEIGHT)]

    def spawn_elements(self):
        '''
            Randomly spawn hills and clouds.

            Args:
                None

            Returns:
                None
        '''
        cloud_dice = random.randint(0, 5)
        hill_dice = random.randint(0, 3)

        if cloud_dice == 3:
            self.__clouds.append(cloud(random.randint(0, cloud.n_types - 1)))

        if hill_dice == 1:
            self.__clouds.append(hill(random.randint(0, hill.n_types - 1)))

    def clear_elements(self):
        '''
            Remove elements that have passed outside the left of the screen.

            Args:
                None

            Returns:
                None
        '''
        if len(self.__clouds) > 0 and self.__clouds[0].out_of_screen():
            self.__clouds.pop(0)

        if len(self.__hills) > 0 and self.__hills[0].out_of_screen():
            self.__hills.pop(0)

    def draw_grid(self):
        '''
            Draw the sky and the ground, and lay background elements on
            self.grid.

            Args:
                None

            Returns:
                None
        '''
        for i in range(SCREEN_HEIGHT):
            for j in range(SCREEN_WIDTH):
                self.grid[i][j] = None

        for i in range(SCREEN_HEIGHT):
            for j in range(SCREEN_WIDTH):
                if not self.grid[i][j]:
                    if i <= SKY_SIZE:
                        self.grid[i][j] = Background.sky_colour
                    # elif i == SKY_SIZE:
                    #     self.grid[i][j] = Back.BLACK
                    else:
                        self.grid[i][j] = Background.ground_colour

        for cloud in self.__clouds:
            if not cloud.out_of_screen():
                cloud.draw_on_grid(self)

        for hill in self.__hills:
            if not hill.out_of_screen():
                hill.draw_on_grid(self)

    def update(self, dir):
        '''
            Update the background when the user moves by dir steps.

            Args:
                dir : integer

            Returns:
                None
        '''
        if dir < 0:
            # Spawn elements only when moving forward
            self.spawn_elements()

        for cloud in self.__clouds:
            cloud.move(dir)

        for hill in self.__hills:
            hill.move(dir)

        self.draw_grid()
